f = open("aoc/day20.txt", mode="rt")

#give every element its index in the "shuffle queue"
pos = []
for i, x in enumerate(f):
    pos.append((i, int(x)))

length = len(pos)

for i in range(0, length):
    for j in range(0, length):

        #double loop to ensure, every element is moved exactly once
        if i == pos[j][0]:
            pop = pos.pop(j)
            pos.insert((j + pop[1]) % (length - 1), pop)        # -1, because of pop
            break

zero = 0

for (i, val) in enumerate(pos):
    if val[1] == 0:
        zero = i
        break

sum = pos[(zero + 1000) % len(pos)][1] + pos[(zero + 2000) % len(pos)][1] + pos[(zero + 3000) % len(pos)][1]

print(sum)



###Part 2### copy paste

f = open("aoc/day20.txt", mode="rt")

pos = []
for i, x in enumerate(f):
    pos.append((i, int(x) * 811589153)) # constant from task

length = len(pos)

#shuffle the numbers 10 times instead of once

for k in range(0, 10):
    #print("alive")
    for i in range(0, length):
        for j in range(0, length):

            if i == pos[j][0]:
                pop = pos.pop(j)
                pos.insert((j + pop[1]) % (length - 1), pop)
                break

zero = 0

for (i, val) in enumerate(pos):
    if val[1] == 0:
        zero = i
        break

sum = pos[(zero + 1000) % len(pos)][1] + pos[(zero + 2000) % len(pos)][1] + pos[(zero + 3000) % len(pos)][1]

print(sum)