f = open("aoc/day21.txt", mode="rt").readlines()

monkeys = dict()

#use name as key, rest as value
for line in f:
    name, calc = line.strip().split(": ")
    calc = calc.split()
    monkeys[name] = calc


def traverse(monkey):

    calc = monkeys[monkey]

    #termination criteria: only one value instead of 3
    if len(calc) == 1:
       return calc[0]

    x, op, y = calc

    return eval( str(traverse(x)) + op + str(traverse(y)) )

print(traverse("root"))


#######################################


#path down all monkeys till you find yourself. ur down the left side appends True, down the right, false

def find(n):
    calc = monkeys[n]
    if len(calc) == 1:
        return n == 'humn'

    if find(calc[0]):
        monkeys[n].append(True);
        return True

    elif find(calc[2]):
        monkeys[n].append(False);
        return True

    else:
        return False



#check which side ur at, calculate the other side, call recursively with 'humn side' and the solution, which the 'humn side' should have
def trav2(n,x):

    calc = monkeys[n]

    if len(calc) == 1: 
        return x


    if calc[3]:

        #refuses to take match-case statement for some reason 
        #found out they work. my ide just marks them as errors
        op = calc[1]
        left = calc[0]
        y = int(traverse(calc[2]))

        # eval works no longer due to reverse operations

        if(op == '+'):
            return trav2(left, x - y)

        elif(op == '-'):
            return trav2(left, x + y)

        elif(op == '*'):
            return trav2(left, x / y)

        elif(op == '/'):
            return trav2(left, x * y)

    else:
        y = int(traverse(calc[0]))
        op = calc[1]
        right = calc[2]

        if(op == '+'):
            return trav2(right, x - y)

        elif(op == '-'):
            return trav2(right, y - x)

        elif(op == '*'):
            return trav2(right, x / y)

        elif(op == '/'):
            return trav2(right, y / x)



find('root')
calc = monkeys['root']
res = 0
if calc[3]:
    res = trav2(calc[0],traverse(calc[2]))

else:
    res = trav2(calc[2],traverse(calc[0]))

print(res)