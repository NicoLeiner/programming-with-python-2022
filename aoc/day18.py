f = open("aoc/day18.txt", mode="rt").readlines()

touchCounter = 0
cubes = []
for line in f:
    l = line.split(',')
    cubes.append((int(l[0]), int(l[1]), int(l[2])))


for i in range(0, len(cubes) - 1):
    #all others are already checked
    for j in range(i + 1, len(cubes)):
        #check on both sides

        #X
        if (abs(cubes[i][0] - cubes[j][0]) == 1 and cubes[i][1] == cubes[j][1] and cubes[i][2] == cubes[j][2]):
            touchCounter += 1

        #Y
        elif (cubes[i][0] == cubes[j][0] and abs(cubes[i][1] - cubes[j][1]) == 1 and cubes[i][2] == cubes[j][2]):
            touchCounter += 1

        #Z
        elif(cubes[i][0] == cubes[j][0] and cubes[i][1] == cubes[j][1] and abs(cubes[i][2] - cubes[j][2]) == 1):
            touchCounter += 1

print(6 * len(cubes) - 2 * touchCounter)
#6 sides - 2 times touches 


#part 2 some other day
# or not