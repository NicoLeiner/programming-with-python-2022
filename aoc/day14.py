f = open("aoc/day14.txt", mode="rt").readlines()
counter = 0

paths = []
for line in f:
    section = []

    for wall in line.strip().split(" -> "):
        for piece in [wall.split(",")]:
            section.append((int(piece[0]), int(piece[1])))

    paths.append(section)



#we start with an empty cave
#False = empty , True = sand or rock
#size 1000 * 1000 was guessed + observed from input with a large buffer, for other inputs
cave = []
for i in range(0, 1000):
    cave.append([])
    for j in range(0, 1000):
        cave[i].append(False)


########## build cave #############

#lowest low, after which nothing comes anymore
void = 0

for wall in paths:
    for corner in range(0, len(wall) - 1):
        start = wall[corner]
        end = wall[corner + 1]

        lowest = max(start[1], end[1]) + 1
        highest = min(start[1], end[1])
        left = min(start[0], end[0])
        right = max(start[0], end[0]) + 1

        for x in range(left, right):
            for y in range(highest, lowest):
                cave[y][x] = True

        if lowest > void:
            void = lowest

######## pour sand from ceiling #############
pouring = True
while pouring:

    counter += 1
    sand = (500, 0)

    while True:
        
        #sand would fall in void -> we're done
        if sand[1] > void:
           pouring = False
           break

        #below is air
        if (cave[sand[1] +1][sand[0]] == False):
            sand = (sand[0], sand[1] + 1)

        #below left is air
        elif (cave[sand[1] + 1][sand[0] - 1] == False):
            sand = (sand[0] - 1, sand[1] + 1)

        #below right is air
        elif (cave[sand[1] + 1][sand[0] + 1] == False):
                sand = (sand[0] + 1, sand[1] + 1)

        #no free space below
        else:
            cave[sand[1]][sand[0]] = True
            break


print(counter-1)

# do 2 some other time
# not needed anymore