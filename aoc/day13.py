#recursive call n elements deep
def compare(left, right):
    l1 = len(left)
    l2 = len(right)
    maxLen = max(l1, l2)

    for i in range(maxLen):

        #check for list size
        if i >= l1:
            return True
        if i >= l2:
            return False

        leftType = type(left[i])
        rightType = type(right[i])

        #both are int -> simple comparison
        if (leftType == int and rightType == int):
            if left[i] < right[i]:
                return True
            if left[i] > right[i]:
                return False

        else:
            #cast leftType to List -> compare
            if(leftType == int):
                res = compare([left[i]], right[i])
                if res is not None:
                    return res

            #cast rightType to List -> compare
            elif(rightType == int):
                res = compare(left[i], [right[i]])
                if res is not None:
                    return res

            #all fine, just compare both lists
            else:
                res = compare(left[i], right[i])
                if res is not None:
                    return res
            #None check to avoid values from comparing 2 empty lists



f = open("aoc/day13.txt", mode="rt").read().split('\n\n')
pairs = []
for pair in f:
    pairs.append(pair.split('\n'))

counter = 0

for n, pair in enumerate(pairs):
    l = eval(pair[0])
    r = eval(pair[1])

    if compare(l, r):
        counter += n + 1


print(counter)