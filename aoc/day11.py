from math import lcm

class monkey:
    def __init__(self, items,  divable ,true , false, calc):
        self.items = items
        self.divable = divable  #number to divide by
        self.true = true        #monkey to jump to if true
        self.false = false      #monkey to jump to if false
        self.counter = 0
        self.calc = calc        #operation to claculate


f = open("aoc/day11.txt", mode="rt").read().split('\n\n')
monkeys = list()

for m in f:

    mo = m.split('\n')

    temp = mo[1].replace("Starting items: ", '').strip().split(", ")
    li = list()
    for element in temp:
        li.append(int(element))

    op = mo[2].replace('Operation: new = ','').replace('old','item').strip()
    div = int(mo[3].replace('Test: divisible by ', '').strip())
    true = int(mo[4].replace("If true: throw to monkey ", '').strip())
    false = int(mo[5].replace("If false: throw to monkey ", '').strip())

    print(op)
    print(div)
    print(true)
    print(false)
    print(li)

    monkeys.append(monkey(li, div, true, false, op))

for round in range(0,20):
    for monke in monkeys:
        for item in monke.items:

            new = eval(monke.calc) // 3 #eval transforms string in operation

            if (new % monke.divable == 0):
                monkeys[monke.true].items.append(new)

            else:
                monkeys[monke.false].items.append(new)

            monke.counter += 1

        monke.items.clear()#clear list at the end, since monkey threw away everything.



fst = 0
scnd = 0
for monke in monkeys:
    if monke.counter > fst:
        scnd = fst
        fst = monke.counter
    elif monke.counter > scnd:
        scnd = monke.counter

print(fst * scnd)

# task 2 copy paste with small change. use kleinsterGemeinsamerTeiler to keep the number low

f = open("aoc/day11.txt", mode="rt").read().split('\n\n')
monkeys = list()

for m in f:

    mo = m.split('\n')

    temp = mo[1].replace("Starting items: ", '').strip().split(", ")
    li = list()
    for element in temp:
        li.append(int(element))

    op = mo[2].replace('Operation: new = ','').replace('old','item').strip()
    div = int(mo[3].replace('Test: divisible by ', '').strip())
    true = int(mo[4].replace("If true: throw to monkey ", '').strip())
    false = int(mo[5].replace("If false: throw to monkey ", '').strip())

    print(op)
    print(div)
    print(true)
    print(false)
    print(li)

    monkeys.append(monkey(li, div, true, false, op))


kleinsterGemeinsamerTeiler = 1
for monke in monkeys:
    kleinsterGemeinsamerTeiler = lcm(kleinsterGemeinsamerTeiler, monke.divable)

for round in range(0,10000):
    for monke in monkeys:
        for item in monke.items:

            new = eval(monke.calc) % kleinsterGemeinsamerTeiler

            if (new % monke.divable == 0):
                monkeys[monke.true].items.append(new)

            else:
                monkeys[monke.false].items.append(new)

            monke.counter += 1

        monke.items.clear()

fst = 0
scnd = 0
for monke in monkeys:
    if monke.counter > fst:
        scnd = fst
        fst = monke.counter
    elif monke.counter > scnd:
        scnd = monke.counter

print(fst * scnd)