f = open("aoc/day05.txt", mode="rt")
lines = f.read().split("\n")

# even more cursed
l1 = []
l2 = []
l3 = []
l4 = []
l5 = []
l6 = []
l7 = []
l8 = []
l9 = []

for i in range(7, -1, -1):
    
    line = lines[i]
    if line[1] != ' ':
        l1.append(line[1])
    if line[5] != ' ':
        l2.append(line[5])
    if line[9] != ' ':
        l3.append(line[9])
    if line[13] != ' ':
        l4.append(line[13])
    if line[17] != ' ':
        l5.append(line[17])
    if line[21] != ' ':
        l6.append(line[21])
    if line[25] != ' ':
        l7.append(line[25])
    if line[29] != ' ':
        l8.append(line[29])
    if line[33] != ' ':
        l9.append(line[33])

StackList = [l1.copy(), l2.copy(), l3.copy(), l4.copy(), l5.copy(), l6.copy(), l7.copy(), l8.copy(), l9.copy()]


# movement
movement = lines[10:514]
filteredMovement = []
for line in movement:
    line = list(line.split(" "))
    filteredMovement.append((line[1], line[3], line[5]))



for line in filteredMovement:
    #take said amount of items from stack and put them on other stack
    for i in range(0,int(line[0])):
        item = StackList[int(line[1])-1].pop()
        StackList[int(line[2])-1].append(item)

for stack in StackList:
    print(stack.pop())

print("\n\n\n")

##retain order, just do zwischenstack, copy paste code


StackList2 = [l1, l2, l3, l4, l5, l6, l7, l8, l9]
zwischenstack = []

for line in filteredMovement:
    #put them first on zwischenstack and then on correct stack to retain order
    for i in range(0,int(line[0])):
        item = StackList2[int(line[1])-1].pop()
        zwischenstack.append(item)


    for i in range(0, int(line[0])):
        item = zwischenstack.pop()
        StackList2[int(line[2])-1].append(item)
    zwischenstack.clear()

for stack in StackList2:
    print(stack.pop())