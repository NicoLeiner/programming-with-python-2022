def translate(char):
    number = ord(char)

    #lowercase
    if(number > 95 ):
        return number - 96 #1 - 26

    #uppercase
    else: 
        return number - 65 + 27 #27 - 52

f = open("aoc/day03.txt", mode="rt")
rucksacks = f.read().split("\n")

counter = 0

for rucksack in rucksacks:

    #put first half of items left, other half right
    split = len(rucksack) // 2
    left = rucksack[:split]
    right = rucksack[split:]

    # simply check which is in both compartments and add it's prio on the counter
    for item in left:
        if item in right :
            counter += translate(item)
            break

print(counter)

counter2 = 0

# create lists of three elves
a = len(rucksacks) // 3
zipped = []
for i in range(0, a):
    zipped.append((rucksacks[i*3], rucksacks[i*3+1], rucksacks[i*3+2]))


for group in zipped:
    # convert each elf in a set to let python do the work
    item = (set(group[0]) & set(group[1]) & set(group[2])).pop()  # pop(), since intersection() returns another set
    number = translate(item)
    counter2 += number

print (counter2)