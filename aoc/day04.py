f = open("aoc/day04.txt", mode="rt")

pairs = f.read().split("\n")
newpairs = []

# get the 4 needed values
for pair in pairs:

    split = pair.replace('-',',').split(',')
    newpairs.append(split)


counter = 0

#check if one is contained by the other
for pair in newpairs:
    if(int(pair[0]) <= int(pair[2])) and (int(pair[1]) >= int(pair[3])):
        counter += 1

    elif (int(pair[0]) >= int(pair[2])) and (int(pair[1]) <= int(pair[3])):
        counter += 1

print(counter)


counter2 = 0

#check for overlap
for pair in newpairs:
    if(int(pair[0]) <= int(pair[2])) and (int(pair[2]) <= int(pair[1])):
        counter2 += 1
    elif (int(pair[0]) <= int(pair[3])) and (int(pair[3]) <= int(pair[1])):
        counter2 += 1
    elif (int(pair[2]) <= int(pair[0])) and (int(pair[0]) <= int(pair[3])):
        counter2 += 1
    elif (int(pair[2]) <= int(pair[1])) and (int(pair[1]) <= int(pair[3])):
        counter2 += 1

print(counter2)

