f = open("aoc/day15.txt", "r").readlines()
coordlist = []
#set to avoid duplicates
covered = set()

#get coordinates
for line in f:
    l = line.replace("Sensor at ", "").replace("x=", "").replace(",", " ").replace("y=", "").replace(": closest beacon is at ", " ").split()
    coordlist.append((int(l[0]), int(l[1]), int(l[2]), int(l[3])))


for coords in coordlist:
    maxdist = abs(coords[0] - coords[2]) + abs(coords[1] - coords[3])
    rest = maxdist - abs(2000000 - coords[1])

    #won't reach our 2000000 line
    if rest <= 0:
        continue

    # else add all the reachable coords in the list
    for i in range(-rest, rest):
        covered.add(coords[0] + i)


print(len(covered))

#part 2 later
#nop