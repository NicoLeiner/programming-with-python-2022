translate = {'0': 0, '1': 1, '2': 2, '-': -1, '=': -2}
reverse = {0: '0', 1: '1', 2: '2', -1 : '-', -2 : '='}

def convBack(a):
    #breaking condition
    if a == 0:
        return ''

    #quotient and remainder +2 for 0 to 4
    quo = (a + 2) // 5
    rem = ((a + 2) % 5) - 2


    return convBack(quo) + reverse[rem]


f = open("aoc/day25.txt", mode="rt").read().splitlines()

counter = 0
for line in f:
    #translate in base 0
    val = 0
    for d in line:
       val = val * 5 + translate[d]
    counter += val

print(convBack(counter))