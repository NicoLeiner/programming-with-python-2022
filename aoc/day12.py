# watered down version of dijkstra algorithm
def traverse(start):
    #write -1 on a distance-map for every field (not traversed yet)
    distance = []
    for i in range(0,len(f)):
        row = []
        for j in range(0,len(f[0])):
            row.append(-1)
        distance.append(row)


    # start to 0, put in queue
    distance[start[0]][start[1]] = 0
    queue = [(start[0], start[1])]


    while queue:
        row, col = queue.pop(0)
        for y, x in dirs:

            nextrow = row + y
            nextcol = col + x

            # check if inbounds
            if 0 <= nextrow and nextrow < len(f) and 0 <= nextcol and nextcol < len(f[0]):
                #check if already traversed and viewed field is lower than current
                if distance[nextrow][nextcol] == -1 and elevation(nextrow, nextcol) - 1 <= elevation(row, col):

                    distance[nextrow][nextcol] = distance[row][col] + 1
                    queue.append((nextrow, nextcol))

    return distance[end[0]][end[1]]

# cannot change var in string easy, so I just check for S and E
def elevation(row, col):

    if f[row][col] == 'S':
        return ord('a')

    if f[row][col] == 'E':
        return ord('z')

    return ord(f[row][col])


f = open("aoc/day12.txt", mode="rt").read().splitlines()
start = (0,0)
end = (0,0)

#get coords of start and end
for y, row in enumerate(f):
    for x, pos in enumerate(row):
        if pos == 'S':
            start = (y, x)
        if pos == 'E':
            end = (y, x)

dirs = ((-1, 0), (1, 0), (0, -1), (0, 1))


print(traverse(start))

#since we need to traverse a 'b' and all bs are at col 1 and as at 0
best = len(f)*len(f[0])
for y, row in enumerate(f):
    res = traverse((y, 0))
    if res < best:
        best = res

print(best)