f = open("aoc/day23.txt", mode="rt").readlines()

pos = set()
for i, y in enumerate(f):
    for j, x in enumerate(y.strip()):
        if x == '#':
            pos.add((i,j))


#adjacent tiles for elf
adj = ((-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1),)
#proposed and checked dirs
dirs = [((-1, 0), ((-1, -1),(-1, 0),(-1, 1))),   ((1, 0), ((1, -1),(1, 0),(1, 1))),   ((0, -1), ((-1,-1),(0, -1),(1, -1))),    ((0, 1),((-1, 1),(0, 1),(1, 1)))]

#proposed new positions from elves
propPos = dict()
count = dict()


moveCounter = 0
roundCounter = 0

while True:
    moveCounter = 0
    roundCounter += 1

    for elf in pos:

        #check if elf is lonely
        if all((elf[0] + dir[0], elf[1] + dir[1]) not in pos for dir in adj):
            continue


        # go through all possibe directions,
        # check each time if directions are free -> if so, add proposition to dict and break
        for prop, check in dirs:
            if all((elf[0] + dir[0], elf[1] + dir[1])not in pos for dir in check):
                propPos[elf] = (elf[0] + prop[0], elf[1] + prop[1])
                break


    #get amount of elves that want to move to one tile
    for val in propPos.values():
        if val in count:
            count[val] += 1
        else:
            count[val] = 1

    #if Position exists only once, elf is allowed to move
    for elf, prop in propPos.items():
        if count[prop] == 1:
            pos.remove(elf)
            pos.add(prop)
            moveCounter += 1

    #move first direction to the end
    temp = dirs.pop(0)
    dirs.append(temp)
    #cleanup
    propPos.clear()
    count.clear()


    #print(moveCounter, roundCounter)
    #Part 1
    if(roundCounter == 10):
        mini = 10000
        maxi = -10000
        minj = 10000
        maxj = -10000



        # get min, max, of bbox
        for elf in pos:
            mini = min(mini, elf[0])
            maxi = max(maxi, elf[0])
            minj = min(minj, elf[1])
            maxj = max(maxj, elf[1])

        print((maxi - mini + 1) * (maxj - minj + 1) - len(pos))

    #Part 2
    if(moveCounter == 0):
        print(roundCounter)
        break