f = open("aoc/day08.txt", mode="rt")
rows = f.read().splitlines()

counter = 0
mainscore = 0

for x in range(len(rows)):
    for y in range(len(rows[0])):
        #all directions for each tree
        tree = rows[x][y]
        left = rows[x][:y]
        right = rows[x][y+1:len(rows[0])]
        up = [rows[pos][y] for pos in range(0,x)]
        down = [rows[pos][y] for pos in range(x+1,len(rows))]
        

        for dir in left, right, up, down:
            #visible
            if all(t < tree for t in dir):
                counter += 1
                break


        score = 1
        #flip to get the view from treehouse
        left_flip = left[::-1]
        up_flip = up[::-1]

        for dir in left_flip, right, up_flip, down:
            hit = False
            #enum for dist between treehouse and hit tree
            for other_tree in enumerate(dir):
                if tree <= other_tree[1]:
                    hit = True
                    score = score * (other_tree[0]+1); 
                    break

            #can look outside
            if not hit and len(dir) != 0:
                score = score * len(dir)

              
        if(score > mainscore):
            mainscore = score


print(counter, mainscore)