f = open("aoc/day07.txt", mode="rt").read()
text = f.splitlines()


dirs = dict()

#use list for dir for easy .. and 

current_dir = ['']
for line in text:
    args = line.split()
    if args[0] == '$':
        if args[1] == 'cd':
            if args[2] == '/':
                # go to the top
                current_dir = ['']
            elif args[2] == '..':
                #pop one folder up
                current_dir.pop()
            else:
                # go deeper
                current_dir.append(args[2])


    elif args[0].isdigit():

        # get all the directories
        keys = ['']
        for item in current_dir[1:]:
            keys.append(keys[-1] + item)

        # add the value of the file in them
        for key in keys:
            if key not in dirs:
                dirs[key] = 0
            dirs[key] += int(args[0])



counter = 0
#add up val of all dirs smaller than 100000
for val in dirs.values():
    if val  <= 100000:
        counter += val
        
print(counter)


space = 70000000 - 30000000
minVal = dirs['']
total = minVal

# find smallest dir, greater than space
for val in dirs.values():
    if val > total - space and val < minVal:
        minVal = val

print(minVal)