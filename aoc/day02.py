f = open("aoc/day02.txt", mode="rt")
matches = f.read().split("\n")
score = 0
score2 = 0


for round in matches:

    #   fst: rock = 1, paper = 2, scissor = 3, scnd: win = 6, draw = 3, loose = 0, 
    match round:
        case "A X": score += (1 + 3)
        case "A Y": score += (2 + 6)
        case "A Z": score += (3 + 0)
        case "B X": score += (1 + 0)
        case "B Y": score += (2 + 3)
        case "B Z": score += (3 + 6)
        case "C X": score += (1 + 6)
        case "C Y": score += (2 + 0)
        case "C Z": score += (3 + 3)
        case _: print("Error", matches.index(round))

    # take a paper and draw it up when asked A,X rock; B,Y Paper; C,Z scissor; enemy, you
    match round:
        case "A X": score2 += (3 + 0)
        case "A Y": score2 += (1 + 3)
        case "A Z": score2 += (2 + 6)
        case "B X": score2 += (1 + 0)
        case "B Y": score2 += (2 + 3)
        case "B Z": score2 += (3 + 6)
        case "C X": score2 += (2 + 0)
        case "C Y": score2 += (3 + 3)
        case "C Z": score2 += (1 + 6)
        case _: print("Error", matches.index(round))

print(score, score2)
