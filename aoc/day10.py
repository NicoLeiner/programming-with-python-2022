f = open("aoc/day10.txt", mode="rt").readlines()
cycle = 0
reg = 1
counter = 0
output = ''

for instruction in f:
    instruction = instruction.rstrip().split()

    if instruction[0] == 'noop':

        #### Part 2
        # if next cycle mod 40 = 0, newline. else, check if it lies in range 1 to reg. 
        if(cycle + 1) % 40 == 0:
            output += '\n'
        elif(cycle % 40 == reg or cycle % 40 == reg-1 or cycle % 40 == reg+1):
            output += '#'
        else: 
            output += '.'
        ####


        #cycle 1 up, if desired value, add to counter...
        cycle += 1
        if (cycle - 20) % 40 == 0 :
            counter += reg * cycle




    elif instruction[0] == 'addx':


        ##### Part 2
        if(cycle + 1) % 40 == 0:
            output += '\n'
        elif(cycle % 40 == reg or cycle % 40 == reg-1 or cycle  % 40 == reg+1):
            output += '#'
        else: 
            output += '.'
        ####

        cycle += 1
        if (cycle - 20) % 40 == 0 :
            counter += reg * cycle




        #### Part 2
        if(cycle + 1) % 40 == 0:
            output += '\n'
        elif(cycle % 40 == reg or cycle % 40 == reg-1 or cycle  % 40 == reg+1):
            output += '#'
        else: 
            output += '.'
        ####

        cycle += 1
        if (cycle - 20) % 40 == 0 :
            counter += reg * cycle

        reg += int(instruction[1]) 

print(counter)
print(output)