from math import sqrt

f = open("aoc/day09.txt", mode="rt").readlines()

#dict for all directions
translate = {
    'L':(-1,0),
    'R':(1,0),
    'U':(0,1),
    'D':(0,-1)
    }

#lists, because tuple are immutable
head = [0,0]
tail = [0,0]


coords = set()

for line in f:
    vals = line.split(" ")
    for i in range(int(vals[1])):

        #head

        head[0] += translate[vals[0]][0]
        head[1] += translate[vals[0]][1]


        #tail

        #same y axis
        if head[1] == tail[1]:              
            if head[0] - tail[0] > 1:           #head too far to the right
                tail[0] += 1
            elif tail[0] - head[0] >1:          #head too far too the left
                tail[0] += -1
        
        # same x axis
        elif head[0] == tail[0]:            
            if head[1] - tail[1] > 1:           #head too far up
                tail[1] += 1
            elif tail[1] - head[1] >1:          #head too far down
                tail[1] += -1

        #euclidean distance between head and tail > 2, no axis identical => diagonal movement
        elif sqrt((head[0] - tail[0])**2 + (head[1] - tail[1])**2) > 2:     
            if head[0] > tail[0]:           #head too far to the right
                tail[0] += 1
            elif tail[0] > head[0]:          #head too far too the left
                tail[0] += -1
        
            if head[1] > tail[1]:           #head too far up
                tail[1] += 1
            elif tail[1] > head[1]:          #head too far down
                tail[1] += -1


        tuptail = tuple(tail)   #list is unhashable, so we convert to tuple
        coords.add(tuptail)


print(len(coords))

#copy-paste since I'm lazy

nodes = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]

f = open("aoc/day09.txt", mode="rt").readlines()

tail = set()


for line in f:
    vals = line.split(" ")
    for i in range(int(vals[1])):

        nodes[0][0] += translate[vals[0]][0]
        nodes[0][1] += translate[vals[0]][1]

        for i in range(1,10):

            #same y axis
            if nodes[i-1][1] == nodes[i][1]:              
                if nodes[i-1][0] - nodes[i][0] > 1:           #head too far to the right
                    nodes[i][0] += 1
                elif nodes[i][0] - nodes[i-1][0] > 1:          #head too far too the left
                    nodes[i][0] += -1

            # same x axis
            elif nodes[i-1][0] == nodes[i][0]:            
                if nodes[i-1][1] - nodes[i][1] > 1:           #head too far up
                    nodes[i][1] += 1
                elif nodes[i][1] - nodes[i-1][1] >1:          #head too far down
                    nodes[i][1] += -1

            #euclidean distance between head and tail > 2, no axis identical => diagonal movement
            elif sqrt((nodes[i-1][0] - nodes[i][0])**2 + (nodes[i-1][1] - nodes[i][1])**2) > 2:     
                if nodes[i-1][0] > nodes[i][0]:           #head too far to the right
                    nodes[i][0] += 1
                elif nodes[i][0] > nodes[i-1][0]:          #head too far too the left
                    nodes[i][0] += -1
        
                if nodes[i-1][1] > nodes[i][1]:           #head too far up
                    nodes[i][1] += 1
                elif nodes[i][1] > nodes[i-1][1]:          #head too far down
                    nodes[i][1] += -1

            #print(nodes[0])
            if i == 9:
                
                tail.add(tuple(nodes[9]))

print(len(tail))
