f = open("aoc/day06.txt", mode="rt").read()

char1 = 'q'
char2 = 'h'
char3 = 'b'


counter = 1
for char in f:
    #check if all chars are different
    if(char != char1 and char != char2 and char != char3 and char1 != char2 and char2 != char3 and char1 != char3):
        print(counter)
        break

    #move all one char back
    char1 = char2
    char2 = char3
    char3 = char 
    counter += 1



#try set 
for i in range(0, len(f) - 14) :
    chars = f[i : i + 14]
    if((len(set(chars))) == 14):
        print(i + 14)

#set good

#because of set property