f = open("aoc/day01.txt", mode="rt")

#split in packets 
elves = f.read().split("\n\n")

total_calories = []

#sum values of each packet up
for elf in elves:
    foods = elf.strip().split("\n")
    calories = 0
    for food in foods:
        calories += int(food)
    total_calories.append(calories)

#sort
total_calories.sort(reverse=True)


print(total_calories[0])
print(total_calories[0] + total_calories[1] + total_calories[2])
